import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;

import static java.util.concurrent.TimeUnit.SECONDS;

public class Calculator {
    static AppiumDriver<MobileElement> driver;

    public static void main(String[] args) {
        try {
            onBoarding();
        } catch (Exception e) {
            e.getCause();
            e.getMessage();
            e.printStackTrace();
        }

    }


    public static void waitToClick(String findById) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(findById)));
        element.click();
    }

    public static void waitToClickByXPath(String findByXPath) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(findByXPath)));
        element.click();
    }

    public static void waitToSenKey(String findById, String key) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(findById)));
        element.sendKeys(key);

    }

    public static void waitToClickAndSenKey(String findById, String key) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(findById)));
        element.click();
        element.sendKeys(key);
    }
    public static void onBoarding() throws Exception {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("udid", "emulator-5554");
        cap.setCapability("platformName", "Android");
        cap.setCapability("platformVersion", "8");
        cap.setCapability("appPackage", "ch.twint.payment.configurable");
        cap.setCapability("appActivity", "ch.twint.payment.ui.activities.start.StartActivity");
        URL url = new URL("http://127.0.0.1:4723/wd/hub");
        driver = new AppiumDriver<MobileElement>(url, cap);
        System.out.println("run ...");
        MobileElement clickCreateANewAccount = driver.findElement(By.id("ch.twint.payment.configurable:id/btnPrimary"));
        driver.manage().timeouts().implicitlyWait(10, SECONDS);
        clickCreateANewAccount.click();
        waitToClick("ch.twint.payment.configurable:id/btnPrimary");
        waitToClickAndSenKey("ch.twint.payment.configurable:id/phoneNumber", "+41791234567");
        waitToClick("ch.twint.payment.configurable:id/btnPrimary");
        waitToSenKey("ch.twint.payment.configurable:id/token", "11111");
        waitToClick("ch.twint.payment.configurable:id/continueButton");
        waitToSenKey("ch.twint.payment.configurable:id/firstName", "Tran");
        waitToClickAndSenKey("ch.twint.payment.configurable:id/lastName", "Son");
        waitToClick("ch.twint.payment.configurable:id/dateOfBirth");
        waitToClick("ch.twint.payment.configurable:id/continueButton");
        waitToClick("ch.twint.payment.configurable:id/firstName");
        waitToClick("ch.twint.payment.configurable:id/btnPrimary");
        waitToClick("ch.twint.payment.configurable:id/documentIdentityCardImage");
        waitToClickByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]");
        waitToClick("com.android.packageinstaller:id/permission_allow_button");
        waitToClickByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.view.View[3]");
        waitToClick("ch.twint.payment.configurable:id/btnPrimary");
        waitToClickByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.view.View[3]");
        waitToClick("ch.twint.payment.configurable:id/btnPrimary");
        waitToClickAndSenKey("ch.twint.payment.configurable:id/street", "Hanoi");
        waitToClickAndSenKey("ch.twint.payment.configurable:id/streetNumber", "123");
        waitToClickAndSenKey("ch.twint.payment.configurable:id/postalCode", "1234");
        MobileElement city = driver.findElement(By.id("ch.twint.payment.configurable:id/city"));
        city.click();
        city.clear();
        city.sendKeys("hanoi");
        waitToClick("ch.twint.payment.configurable:id/countryDropDown");
        waitToClick("ch.twint.payment.configurable:id/postalCode");
        waitToClick("ch.twint.payment.configurable:id/btnPrimary");
        waitToClick("ch.twint.payment.configurable:id/cbWork");
        waitToClick("ch.twint.payment.configurable:id/btnPrimary");
        waitToClick("ch.twint.payment.configurable:id/onlyMe");
        waitToClick("ch.twint.payment.configurable:id/btnPrimary");
        waitToClickAndSenKey("ch.twint.payment.configurable:id/pin", "111111");
        waitToClickAndSenKey("ch.twint.payment.configurable:id/pinRepeat", "111111");
        waitToClick("ch.twint.payment.configurable:id/btnPrimary");
        waitToClickAndSenKey("ch.twint.payment.configurable:id/email", "01263519039son@gmail.com");
        driver.hideKeyboard();
        waitToClick("ch.twint.payment.configurable:id/btnPrimary");
        waitToClick("ch.twint.payment.configurable:id/yes");
        waitToClick("ch.twint.payment.configurable:id/btnPrimary");
        waitToClick("ch.twint.payment.configurable:id/okayButton");
        waitToClick("com.android.packageinstaller:id/permission_allow_button");
        System.out.println("finish");


    }
}
